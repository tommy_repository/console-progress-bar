public class ProgressBar {
	static void updateProgress(double progressPercentage,String header) {
		final int width = 25;
		System.out.println("\r"+header+"     ");
		System.out.print("[");
		int i = 0;
		for (; i <= (int)(progressPercentage*width); i++) {
			System.out.print("*");
		}
		//System.out.print("  "+progressPercentage+"%");
		for (; i < width; i++) {
			System.out.print(" ");
		}
		progressPercentage *= 100;
		System.out.print("]"+(int)progressPercentage+"%\n");
	}
}
